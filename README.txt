Pour utiliser MSAL pour l´authentification AD (Active Directory) on convertit du code Go en DLL pour l´utiliser dans Artiview. Ce choix est fait car aucune lib MSAL existe en C++.

Pour se faire utiliser le main.go :
- Ouvrir un terminal dans le dossier qui contient le main.go. Initialiser go module :
     go mod init GoAdalDLL
- Récupérer la dépendence git Go ADAL:
    go get github.com/Azure/go-autorest/autorest/adal
- Mettre à jour les dépendances du projet :
    go mod tidy
- Générer la DLL :
    go build -buildmode=c-shared -o GoAdalDLL.dll main.go  

Une fois générée, on obtient un fichier DLL et un header. 
Dans le header il faut supprimer les lignes suivantes : 

    typedef __SIZE_TYPE__ GoUintptr;
    typedef float _Complex GoComplex64;
    typedef double _Complex GoComplex128;


Les instructions sont également disponibles sur le wiki : https://wiki.aquilab.com/index.php?title=MVISION#G.C3.A9n.C3.A9ration_DLL