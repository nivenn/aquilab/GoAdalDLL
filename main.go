package main

import (
	"C"

	"github.com/Azure/go-autorest/autorest/adal"
)

const (
	NoError = iota
	Err1
	Err2
	Err3
)

//export GetAccessToken
func GetAccessToken(activeDirectoryEndpoint *C.char, tenantID *C.char, resource *C.char, clientID *C.char, clientKey *C.char) (*C.char, int) {
	oauthConfig, err := adal.NewOAuthConfigWithAPIVersion(C.GoString(activeDirectoryEndpoint), C.GoString(tenantID), nil)

	if err != nil {
		return C.CString(""), Err1
	}

	callback := func(token adal.Token) error {
		return nil
	}

	// The resource for which the token is acquired​
	spt, err := adal.NewServicePrincipalToken(
		*oauthConfig,
		C.GoString(clientID),
		C.GoString(clientKey),
		C.GoString(resource),
		callback)
	if err != nil {
		return C.CString(""), Err2
	}

	// Acquire a new access token
	err = spt.Refresh()
	if err != nil {
		return C.CString(""), Err3
	}
	token := spt.Token
	return C.CString(token().AccessToken), NoError
}

func main() {
}
